package org.example.question;

public class Math2Question extends AbstractQuestion {


    public Math2Question() {
        super("Вопрос 4. \nНапиши, сколько сантиметров в 1дм и 5см. Отправь ответ цифрой:");
    }

    // Переопределение метода проверки ответа на вопрос.
    @Override
    public boolean checkAnswer(String answer) {
        return answer.equals("15");
    }
}