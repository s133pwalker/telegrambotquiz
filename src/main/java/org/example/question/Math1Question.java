package org.example.question;

public class Math1Question extends AbstractQuestion {


    public Math1Question() {
        super("Вопрос 1. \nРеши задачу: У Васи было 7 значков, а у Вовы на 4 больше. Сколько всего значков у мальчиков? Отправь ответ цифрой:");
    }

    // Переопределение метода проверки ответа на вопрос.
    @Override
    public boolean checkAnswer(String answer) {
        return answer.equals("18");
    }
}