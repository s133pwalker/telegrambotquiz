package org.example.question;

public class RusQuestion extends AbstractQuestion {

    private String[] methods = {"козы", "козочка", "козий", "козлик", "козочки"};

    public RusQuestion(){
        super("Вопрос 3. \nПодбери проверочное слово к слову «к…за»");

    }

    // Переопределение метода проверки ответа на вопрос.
    @Override
    public boolean checkAnswer(String answer) {
        answer = answer.toLowerCase(); // Приведение ответа к нижнему регистру для сравнения без учета регистра.

        // Проверка, содержит ли ответ ключевые слова из списка methods.
        for (String method : methods) {
            if (answer.contains(method)) {
                return true; // Если найдено хотя бы одно ключевое слово, возвращается true.
            }
        }

        return false; // Если ни одно из ключевых слов не найдено в ответе, возвращается false.
    }
}