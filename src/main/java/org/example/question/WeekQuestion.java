package org.example.question;

public class WeekQuestion extends AbstractQuestion{

    public WeekQuestion() {
        super("Вопрос 2. \nНазови день недели, который получил свое название потому, что находится в середине недели.");
    }

    // Переопределение метода проверки ответа на вопрос.
    @Override
    public boolean checkAnswer(String answer) {
        return answer.toLowerCase().contains("среда");
    }
}