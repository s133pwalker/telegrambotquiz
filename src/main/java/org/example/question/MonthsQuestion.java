package org.example.question;

public class MonthsQuestion extends AbstractQuestion {

    private String[] methods = {"июнь", "июль", "август"};

    public MonthsQuestion() {
        super("Вопрос 5. \nНапиши названия летних месяцев года:");
    }

    // Переопределение метода проверки ответа на вопрос.
    @Override
    public boolean checkAnswer(String answer) {
        answer = answer.toLowerCase();  // Приведение ответа к нижнему регистру для сравнения без учета регистра.

        // Проверка, содержит ли ответ все ключевые слова из списка methods.
        for (String method : methods) {
            if (!answer.contains(method)) {
                return false; // Если хотя бы одного ключевого слова нет в ответе, возвращается false.
            }
        }

        return true; // Если все ключевые слова присутствуют в ответе, возвращается true.
    }

}