/*
Данный класс представляет собой основную логику бота, который взаимодействует с пользователями через Telegram и проводит викторину.
Вот краткое описание его функционала:
1.Подключение необходимых библиотек и классов:
    Импортируются классы из пакетов org.example.question и org.telegram.telegrambots.
    Bot наследует TelegramLongPollingBot, что обеспечивает его работу как Telegram-бота, способного обрабатывать обновления.
2.Переменные класса:
    users: Хранит данные пользователей по их идентификаторам.
    questions: Список вопросов для викторины.
    teacherChatId: ChatID учителя, необходим для отправки результатов ученикам.
3.Конструктор класса Bot():
    Инициализирует переменные users и questions.
    Добавляет в список questions объекты вопросов различных типов (например, Math1Question, WeekQuestion).
4.Методы getBotUsername() и getBotToken():
    Возвращают имя и токен бота соответственно.
5.Метод sendText():
    Отправляет текстовые сообщения пользователю.
6.Метод sendResultsToTeacher():
    Формирует сообщение с результатами участника викторины и отправляет его учителю.
7.Метод onUpdateReceived():
    Обрабатывает полученные обновления (сообщения от пользователей).
    Если получено сообщение "/start", запрашивает Фамилию и Имя у пользователя и начинает викторину.
    Сохраняет ответы пользователя, подсчитывает баллы и отправляет следующий вопрос.
    По завершении викторины отправляет результаты пользователю и учителю.
*/

package org.example;

import org.example.question.*;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Bot extends TelegramLongPollingBot {

    // Хранит данные пользователей по их идентификаторам
    private final HashMap<Long, UserData> users;

    // Список вопросов для викторины
    private final ArrayList<AbstractQuestion> questions;

    // ChatID учителя, нужен для отправки результатов ученика
    private final long teacherChatId;

    public Bot() {
        users = new HashMap<>();
        questions = new ArrayList<>();
        // Добавление вопросов в список
        questions.add(new Math1Question());
        questions.add(new WeekQuestion());
        questions.add(new RusQuestion());
        questions.add(new Math2Question());
        questions.add(new MonthsQuestion());
        // Инициализация СhatID учителя (заменить на реальный ID)
        teacherChatId = 1486566681;
    }

    @Override
    public String getBotUsername() {
        return "Pervoklashka"; // Имя бота
    }

    @Override
    public String getBotToken() {
        return "6757543264:AAF8hGRDmWGjRs1BpKKTdFoXzpiHQm9i3ME"; // Токен бота
    }

    // Отправка текстового сообщения
    public void sendText(Long chatId, String message) {
        SendMessage sm = new SendMessage();
        sm.setChatId(chatId.toString());
        sm.setText(message);
        try {
            execute(sm);
        } catch (TelegramApiException e) {
            throw new RuntimeException(e);
        }
    }

    // Отправка результатов учителю
    public void sendResultsToTeacher(UserData userData) {
        StringBuilder resultMessage = new StringBuilder("Результат участника " + userData.getName() + ":\n" +
                "Набрано баллов: " + userData.getScore() + "/" + questions.size() +
                "\nТелеграм-аккаунт участника: @" + userData.getTelegramUsername() + "\n");
        //Вывод ответов, которые дал ученик
        List<String> answers = userData.getAnswers();
        for (int i = 0; i < answers.size(); i++) {
            resultMessage.append("Ответ ").append(i + 1).append(": \"").append(answers.get(i)).append("\"\n");
        }

        sendText(teacherChatId, resultMessage.toString());
    }

    @Override
    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();
        String text = message.getText();
        long userId = message.getFrom().getId();
        long chatId = message.getChatId();
        String username = message.getFrom().getUserName(); // Получение имени пользователя

        if (text.equals("/start")) {
            // Запрос Фамилии и Имени у пользователя
            sendText(userId, "Привет! Напиши свою Фамилию и Имя: ");
            // Создание новой записи для пользователя
            users.put(userId, new UserData());
        } else if (!users.containsKey(userId) || users.get(userId).getName() == null) {
            // Сохранение Фамилии и Имени пользователя
            users.get(userId).setName(text);
            users.get(userId).setTelegramUsername(username); // Сохранение имени пользователя в Telegram
            System.out.println("Имя участника викторины: " + text + "\n" +
                    "Телеграм-аккаунт участника: @" + username + "\n" + //Вывод информации о пользователе в консоль разработчика
                    "Chat ID: " + chatId + "\n");
            // Приветствие и объяснение процесса викторины
            sendText(userId, "Приятно познакомиться, " + text + "! Сейчас тебе будут заданы 5 вопросов.");
            // Отправка первого вопроса
            String question = questions.get(0).getQuestion();
            sendText(userId, question);
        } else if (users.get(userId).getQuestionNumber() == questions.size()) {
            // Уведомление об окончании викторины и отправка результатов учителю
            sendText(userId, "Тест завершён. Для перезапуска бота используйте команду /start");
            sendResultsToTeacher(users.get(userId)); // Отправка результатов учителю
        } else {
            UserData userData = users.get(userId);
            int questionNumber = userData.getQuestionNumber();
            boolean result = questions.get(questionNumber).checkAnswer(text);
            int score = userData.getScore();
            int nextQuestion = questionNumber + 1;
            userData.setScore(score + (result ? 1 : 0));
            userData.setQuestionNumber(nextQuestion);
            userData.addAnswer(text); // Сохранение ответа пользователя

            if (nextQuestion == questions.size()) {
                // Уведомление о завершении и отправка результатов учителю
                sendText(userId, "Ваш рейтинг: " + users.get(userId).getScore() + " из " + questions.size() + "." + "\nДля перезапуска бота используйте команду /start");
                sendResultsToTeacher(users.get(userId)); // Отправка результатов учителю
            } else {
                // Отправка следующего вопроса
                String question = questions.get(nextQuestion).getQuestion();
                sendText(userId, question);
            }
        }
    }
}