/*
Этот класс представляет собой точку входа в приложение.
Он создает экземпляр TelegramBotsApi, который используется для регистрации и запуска Telegram-бота.
В методе main происходит регистрация бота с помощью botsApi.registerBot(new Bot()),
где Bot() - это экземпляр класса Bot, реализующего логику работы бота.
*/

package org.example;

import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

// Основной класс приложения, который инициализирует и запускает Telegram-бота.
public class App {

    // Метод main, точка входа в приложение.
    public static void main(String[] args) throws TelegramApiException {

        // Создание экземпляра TelegramBotsApi для работы с Telegram API.
        TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);

        // Регистрация и запуск Telegram-бота.
        botsApi.registerBot(new Bot());
    }
}