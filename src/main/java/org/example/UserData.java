/*
Этот класс предоставляет удобный способ хранения и доступа к информации о пользователе,
такой как его имя, username в Telegram, текущий результат викторины и список ответов на вопросы.
Вот его основной функционал:
1.Переменные класса:
    questionNumber: Хранит номер текущего вопроса, на который отвечает пользователь.
    score: Хранит текущий результат пользователя.
    name: Хранит имя пользователя.
    telegramUsername: Хранит username пользователя в Telegram.
    answers: Список для хранения ответов пользователя.
2.Конструктор класса:
    Инициализирует переменные questionNumber и score нулевыми значениями.
    Создает новый пустой список answers.
3.Геттеры и сеттеры:
    Позволяют получать и устанавливать значения для переменных класса.
4.Метод addAnswer():
Добавляет ответ пользователя в список ответов.
 */

package org.example;

import java.util.List;
import java.util.ArrayList;

public class UserData {
    // Переменные для хранения номера вопроса, текущего результата, имени пользователя и его username в Telegram
    private int questionNumber;
    private int score;
    private String name;
    private String telegramUsername;
    // Список для хранения ответов пользователя
    private List<String> answers;

    // Конструктор класса, инициализирующий переменные
    public UserData() {
        this.questionNumber = 0;
        this.score = 0;
        this.answers = new ArrayList<>(); // Создание нового списка для ответов
    }

    // Геттеры и сеттеры для доступа к переменным класса
    public int getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        this.questionNumber = questionNumber;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelegramUsername() {
        return telegramUsername;
    }

    public void setTelegramUsername(String telegramUsername) {
        this.telegramUsername = telegramUsername;
    }

    public List<String> getAnswers() {
        return answers;
    }

    // Метод для добавления ответа пользователя в список
    public void addAnswer(String answer) {
        answers.add(answer);
    }
}